package main

import "fmt"

func main() {
	var x string
	x = "This is some test string!"
	y := 5
	z := 4
	fmt.Println(y, "+", z, "=", y+z)
	fmt.Println(x)
	fmt.Println("Hello, World")
	fmt.Println("1 + 1 =", 1+1)
	fmt.Println("1 + 1 =", 1.0+1.0)
	fmt.Println("32132 x 42452 =", 32132*42452)
	fmt.Println(len("Hello, World"))
	fmt.Println("Hello, World"[1])
	fmt.Println("Hello, " + "World")
	fmt.Println("true and true is:")
	fmt.Println(true && true)
	fmt.Println("true and false is:")
	fmt.Println(true && false)
	fmt.Println("true or true is:")
	fmt.Println(true || true)
	fmt.Println("true or false is:")
	fmt.Println(true || false)
	fmt.Println("not true is:")
	fmt.Println(!true)
	fmt.Println("(true && false) || (false && true) || !(false && false) is ",
		(true && false) || (false && true) || !(false && false))

	fmt.Print("Enter a number: ")
	var input float64
	fmt.Scanf("%f", &input)
	output := input * 2
	fmt.Println(output)

}
