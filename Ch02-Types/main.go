package main

import "fmt"

func main() {
  fmt.Println("Integers")
  fmt.Println("1 + 1 =",1+1)
  fmt.Println("Floating points")
  fmt.Println("1 + 1 =",1.0+1.0)
  fmt.Println("Operations with strings:")
  fmt.Println(len("Hello, World"))
  fmt.Println("Hello, World"[1])
  fmt.Println("Hello " + "World")
}
