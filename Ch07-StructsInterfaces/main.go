package main

import (
	"fmt"
	"math"
)

type Circle struct {
	x, y, r float64
}

type Rectangle struct {
	x1, y1, x2, y2 float64
}

func (c *Circle) area() float64 {
	return math.Pi * c.r * c.r
}

func distance(x1, x2 float64) float64 {
	return math.Abs(x1 - x2)
}

func (r *Rectangle) area() float64 {
	l := distance(r.x1, r.x2)
	w := distance(r.y1, r.y2)
	return l * w
}

func main() {
	c := Circle{x: 0, y: 0, r: 5}
	fmt.Println("Circle is defined as: x =", c.x, ", y =", c.y, ", r =", c.r)
	fmt.Println("Circle area is:", c.area())
	r := Rectangle{0, 0, 10, 10}
	fmt.Println("Rectangle is defined as: x1 =", r.x1, ", x2 =", r.x2, ", y1 =", r.y1, ", y2 =", r.y2)
	fmt.Println("Rectangle area is:", r.area())
}
