package main

import (
	"testing"
)

func TestCircle_area(t *testing.T) {
	type fields struct {
		x float64
		y float64
		r float64
	}
	tests := []struct {
		name   string
		fields fields
		want   float64
	}{
		// TODO: write tests
		{"first", fields{0, 0, 1}, 3.141592653589793},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &Circle{
				x: tt.fields.x,
				y: tt.fields.y,
				r: tt.fields.r,
			}
			if got := c.area(); got != tt.want {
				t.Errorf("Circle.area() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_distance(t *testing.T) {
	type args struct {
		x1 float64
		x2 float64
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		// TODO: Add test cases.
		{"first", args{10, 10}, 0},
		{"second", args{0, 10}, 10},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := distance(tt.args.x1, tt.args.x2); got != tt.want {
				t.Errorf("distance() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRectangle_area(t *testing.T) {
	type fields struct {
		x1 float64
		y1 float64
		x2 float64
		y2 float64
	}
	tests := []struct {
		name   string
		fields fields
		want   float64
	}{
		// TODO: Add test cases.
		{"first", fields{0, 0, 10, 10}, 100},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Rectangle{
				x1: tt.fields.x1,
				y1: tt.fields.y1,
				x2: tt.fields.x2,
				y2: tt.fields.y2,
			}
			if got := r.area(); got != tt.want {
				t.Errorf("Rectangle.area() = %v, want %v", got, tt.want)
			}
		})
	}
}
