package main

import "fmt"

func main() {
	var x [5]int
	x[4] = 100
	fmt.Println(x)
	// [0 0 0 0 100]

	var y [5]float64
	y[0] = 98
	y[1] = 93
	y[2] = 77
	y[3] = 82
	y[4] = 83

	var total float64 = 0

	for i := 0; i < 5; i++ {
		total += y[i]
	}
	fmt.Println(total / 5)
	// 86.6

	// using the length of x[]
	total = 0
	for i := 0; i < len(y); i++ {
		total += y[i]
	}
	// we have to convert type here to float64
	fmt.Println(total / float64(len(y)))

	// different approach
	total = 0
	for i, value := range y {
		fmt.Println("The index is now at:", i)
		total += value
	}
	fmt.Println(total / float64(len(y)))

	// different approach
	total = 0
	for _, value := range y {
		total += value
	}
	fmt.Println(total / float64(len(y)))

	//short arrays definition
	z := [5]float64{98, 93, 77, 82, 83}
	fmt.Println(z)

	//or multi-line
	w := [5]float64{
		98,
		93,
		77,
		82,
		83,
	}
	fmt.Println(w)
	fmt.Println("Starting some learning on slices:")
	fmt.Println("_________________________________")
	fmt.Println("")

	// making a slice with length of 5 with capacity of 10,
	// both capacity and length can be omitted
	slice := make([]int, 5, 10)
	slice1 := []int{1, 3, 5, 7, 9}
	fmt.Println("slice =", slice, "slice1 =", slice1)
	slice2 := append(slice1, 11, 13)
	fmt.Println(slice2)
	slice3 := []int{1, 11, 111}
	slice4 := make([]int, 2)
	copy(slice4, slice3)
	fmt.Println("slice3 =", slice3, "=> slice4 =", slice4)
	array1 := [5]float64{1, 2, 3, 4, 5}
	slice5 := array1[0:5]
	slice6 := array1[1:4]
	slice7 := array1[1:]
	slice8 := array1[:4]
	slice9 := array1[:]
	fmt.Println(slice5, slice6, slice7, slice8, slice9)

	fmt.Println("I am testing here:")
	slice10 := make([]int, 5, 10)
	slice10 = append(slice10, 1, 12, 14, 15)
	fmt.Println(slice10)
	slice10 = append(slice10, 1, 12, 14, 15)
	fmt.Println(slice10)

	// moving on to maps
	specialMap := make(map[string]int)
	specialMap["visibility"] = 100
	fmt.Println("visibility =", specialMap["visibility"], "m")

	elements := make(map[string]string)
	elements["H"] = "Hydrogen"
	elements["He"] = "Helium"
	elements["Li"] = "Lithium"
	elements["Be"] = "Beryllium"
	elements["B"] = "Boron"
	elements["C"] = "Carbon"
	elements["N"] = "Nitrogen"
	elements["O"] = "Oxygen"
	elements["F"] = "Fluorine"
	elements["Ne"] = "Neon"
	fmt.Println(elements["Li"])
	if name, ok := elements["F"]; ok {
		fmt.Println(name, ok)
	}
	if name, ok := elements["Un"]; ok {
		fmt.Println(name, ok)
	}
	// more difficult table
	elementsTable := map[string]map[string]string{
		"H": map[string]string{
			"name":  "Hydrogen",
			"state": "gas",
		},
		"He": map[string]string{
			"name":  "Helium",
			"state": "gas",
		},
		"Li": map[string]string{
			"name":  "Lithium",
			"state": "solid",
		},
		"Be": map[string]string{
			"name":  "Beryllium",
			"state": "solid",
		},
		"B": map[string]string{
			"name":  "Boron",
			"state": "solid",
		},
		"C": map[string]string{
			"name":  "Carbon",
			"state": "solid",
		},
		"N": map[string]string{
			"name":  "Nitrogen",
			"state": "gas",
		},
		"O": map[string]string{
			"name":  "Oxygen",
			"state": "gas",
		},
		"F": map[string]string{
			"name":  "Fluorine",
			"state": "gas",
		},
		"Ne": map[string]string{
			"name":  "Neon",
			"state": "gas",
		},
	}
	if el, ok := elementsTable["Li"]; ok {
		fmt.Println(el["name"], el["state"])
	}
	// excercises
	exSlice := []int{1, 3, 5, 7, 9}
	fmt.Println("Printing the 4th element of slice:", exSlice[3])

	lenghtSlice := make([]int, 3, 9)
	fmt.Println("Lenght of the slice is: 3", "(", len(lenghtSlice), ")")

	array2 := [6]string{"a", "b", "c", "d", "e", "f"}
	fmt.Println(array2[2:5])
}
