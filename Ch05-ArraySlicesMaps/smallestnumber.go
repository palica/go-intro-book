package main

import "fmt"

func main() {
	x := []int{
		48, 96, 86, 68,
		57, 82, 63, 70,
		37, 34, 83, 27,
		19, 97, 9, 17,
	}

	// this would go bye bye
	// var lowest int // a place to store the var for comparison
	// this needs to be long style so we can start at 1 instead of zero
	// you don't have "value" defined - does it work?
	// panic: runtime error: index out of range
	// yeah my syntax is wrong somewhere
	// but you see what I'm doing?
	// using x[0] to store the lowest value
	// and want to start the for loop at x[1]
	// yes I follow, you are exchanging the x[0] if it is lower
	// so this would be more efficient in RAM at no cost to CPU
	// interesting, there is also a way to do benchmarking of a code in go
	// we could definitively try it out with those two examples
	// ah that woudl be kewl
	// looking for the book and reference so you could follow with me ....
	// Go recipes.pdf - page 210
	// but we need to learn how to write tests
	// we can do it after we finish this book, what do you think?
	// fixed it
	for i := 1; i < len(x); i++ {
		if x[i] < x[0] {
			x[0] = x[i]
		}
	}
	// prints the contents of "lowest" after the array has been fully iterated over
	fmt.Println("the lowest is:", x[0])
}
