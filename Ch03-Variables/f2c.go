package main

import "fmt"

func main()  {
  fmt.Println("This program converts F to C")
  fmt.Print("Input value in Fahrenheit: ")
  var (
    fahrenheit float64
    celsius float64
  )
  fmt.Scanf("%f", &fahrenheit)
  celsius = (fahrenheit - 32) * 5 / 9
  fmt.Println("Converted value in Celsius: ",celsius)
}
