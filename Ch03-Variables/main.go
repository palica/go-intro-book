package main

import "fmt"

func main()  {
  var x string = "Hello, World"
  fmt.Println(x)
  var y string
  y = "Hello, Universe"
  fmt.Println(y)
  y = "Hello, Deep Space"
  fmt.Println(y)
  var z string = "first "
  fmt.Println(z)
  z = z + "second"
  fmt.Println(z)
  var a string = "hello"
  var b string = "world"
  fmt.Println(a == b)
  c := "This is a string"
  fmt.Println(c)
  d := 5
  fmt.Println("This is a integer",d)
  const e string = "This is some text"
  fmt.Println(e)

  var (
    m = 1
    n = 2
    o = 3
  )
  fmt.Println(m + n + o)
  fmt.Print("Enter number to double: ")
  var input float64
  fmt.Scanf("%f", &input)
  output := input * 2
  fmt.Println(output)

  p := 5
  p += 1
  fmt.Println(p)

  
}
