package main

import "fmt"

func main()  {
  fmt.Print("Input value of feet to convert to meters: ")
  var (
    feet float64
    meters float64
  )
  fmt.Scanf("%f", &feet)
  meters = feet * 0.3048
  fmt.Println(feet,"ft equals to",meters,"m")
}
