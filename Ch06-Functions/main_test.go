package main

import (
	"testing"
)

func TestFibonacci(t *testing.T) {
	type testpair struct {
		value  uint
		result uint
	}

	var tests = []testpair{
		{0, 0},
		{1, 1},
		{2, 1},
		{3, 2},
		{4, 3},
		{5, 5},
		{6, 8},
		{7, 13},
	}

	for _, pair := range tests {
		v := fibonacci(pair.value)
		if v != pair.result {
			t.Error("For", pair.value, "expected", pair.result, "got", v)
		}
	}
}

func TestAverage(t *testing.T) {
	type testpair struct {
		values []float64
		result float64
	}

	var tests = []testpair{
		{[]float64{-1, 0, 1}, 0},
		{[]float64{0}, 0},
		{[]float64{1, 2, 3, 4, 5, 6, 7}, 4},
	}

	for _, pair := range tests {
		v := average(pair.values)
		if v != pair.result {
			t.Error("For", pair.values, "expected", pair.result, "got", v)
		}
	}
}
