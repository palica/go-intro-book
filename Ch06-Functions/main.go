package main

import "fmt"

// basic function
func average(xs []float64) float64 {
	total := 0.0
	for _, v := range xs {
		total += v
	}
	return total / float64(len(xs))
}

// variadic functions
// call the function with zero or more parameters
func sum(nums ...int) int {
	fmt.Println("The sum of ", nums)
	total := 0
	for _, v := range nums {
		total += v
	}
	fmt.Println(" is", total)
	return total
}

// example of a closure
// function calling another function
func makeEvenGenerator() func() uint {
	i := uint(0)
	return func() (ret uint) {
		ret = i
		i += 2
		return
	}
}

func makeOddGenerator() func() uint {
	i := uint(1)
	return func() (ret uint) {
		ret = i
		i += 2
		return
	}
}

// recursion is a call from function to itself
// if x equals to 0 return 1
// return x * call the factorial function again
func factorial(x uint) uint {
	fmt.Println("inside factorial:", x)
	if x == 0 {
		return 1
	}
	return x * factorial(x-1)
}

func fibonacci(n uint) uint {
	if n <= 1 {
		return n
	}
	return fibonacci(n-1) + fibonacci(n-2)
}

// defer, panic and recover
func first() {
	fmt.Println("This is the FIRST function getting called.")
}

func second() {
	fmt.Println("This is the SECOND function getting called.")
}

func panicFunction() {
	defer func() {
		str := recover()
		fmt.Println(str)
	}()
	panic("Not implemented!")
}

// function without pointers
// cannot update a variable
func zeroNotUsingPointer(x int) {
	x = 0
}

// pointers
// using the *int pointer the function is able
// to modify the original variable
func zero(xPtr *int) {
	// When we write *xPtr = 0 , we are saying
	// “store the int 0 in the memory location xPtr refers to.” If we try xPtr = 0 instead, we
	// will get a compile-time error because xPtr is not an int ; it’s a *int , which can only be
	// given another *int .
	*xPtr = 0
	// Finally, we use the & operator to find the address of a variable. &x returns a *int
	// (pointer to an int) because x is an int . This is what allows us to modify the original
	// variable. &x in main and xPtr in zero refer to the same memory location.
}

func main() {
	// parameter names can be different
	// so we could change xs to someOtherName
	xs := []float64{98, 93, 77, 82, 83}
	fmt.Println("Average is: ", average(xs))
	someOtherName := []float64{44, 55, 66, 77}
	fmt.Println("Average is: ", average(someOtherName))
	// now using variadic function
	sum(1, 2, 3)
	nums := []int{1, 2, 3, 4, 5}
	sum(nums...)

	// closure - define function inside function
	add := func(x, y int) int {
		return x + y
	}
	fmt.Println(add(1, 1))

	// another closure example
	x := 0
	increment := func() int {
		x++
		fmt.Println("Incrementing ...")
		return x
	}
	fmt.Println(increment())
	fmt.Println(increment())

	fmt.Println("Even Generator:")
	nextEven := makeEvenGenerator()
	fmt.Println(nextEven()) // 0
	fmt.Println(nextEven()) // 2
	fmt.Println(nextEven()) // 4
	fmt.Println(nextEven()) // 6

	fmt.Println("Odd Generator:")
	nextOdd := makeOddGenerator()
	fmt.Println(nextOdd()) // 0
	fmt.Println(nextOdd()) // 2
	fmt.Println(nextOdd()) // 4
	fmt.Println(nextOdd()) // 6

	var fact uint = 5
	fmt.Println("Factorial of", fact, "is:")
	fmt.Println(factorial(fact))

	defer second()
	first()

	panicFunction()

	x = 5
	fmt.Println("X is now:", x)
	fmt.Println("Zeroing x using the no-pointer-function")
	zeroNotUsingPointer(x)
	fmt.Println("X is now:", x)
	fmt.Println("Zeroing x")
	zero(&x)
	fmt.Println("X is now:", x)

	//Another way to get a pointer is to use the built-in new function
	newPointer := new(int)
	// new takes a type as an argument, allocates enough memory to fit a value of that type,
	// and returns a pointer to it
	*newPointer = 3
	fmt.Println("Pointer is now:", *newPointer)
	zero(newPointer)
	fmt.Println("Pointer is now:", *newPointer)

	fmt.Println("factorial of 3", factorial(3))
	fmt.Println("fibonacci of 10", fibonacci(10))
}
