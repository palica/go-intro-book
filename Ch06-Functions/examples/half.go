package main

import "fmt"

func half(number int) (int, bool) {
	var even bool
	fmt.Println("We are dividing", number)
	if number%2 == 0 {
		even = true
	}
	return number / 2, even
	// this is the solution of the book
	// return number/2, number%2==0
}

func main() {
	fmt.Println("Main function")
	x := 1
	fmt.Println(half(x))
	x = 2
	fmt.Println(half(x))
	x = 3
	fmt.Println(half(x))

}
