// find greatest number from a slice

package main

import "fmt"

func greatest(nums ...int) int {
	x := 0
	for i, value := range nums {
		if i == 0 || value > x {
			x = value
		}
	}
	return x
}

func main() {
	nums := []int{1, 2, 3, 100, 101, 1, 2, 3, 4, 5}
	fmt.Println("Find the greatest number from:", nums)
	fmt.Println(greatest(nums...))
}
