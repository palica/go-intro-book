/*The sequence Fn of Fibonacci numbers is defined by the recurrence relation:
    F n = F n − 1 + F n − 2 , {\displaystyle F_{n}=F_{n-1}+F_{n-2},} {\displaystyle F_{n}=F_{n-1}+F_{n-2},}
with seed values[1][2]
    F 1 = 1 , F 2 = 1 {\displaystyle F_{1}=1,\;F_{2}=1} {\displaystyle F_{1}=1,\;F_{2}=1}
or[5]
    F 0 = 0 , F 1 = 1. {\displaystyle F_{0}=0,\;F_{1}=1.} {\displaystyle F_{0}=0,\;F_{1}=1.}*/

package main

import "fmt"

// The Fibonacci sequence is defined as: fib(0) = 0 , fib(1) = 1 , fib(n) =
// fib(n-1) + fib(n-2) . Write a recursive function that can find fib(n) .

// fib(0) 0 + fib(1) 1 +
// fib(2) = fib(1) + fib (0)
// fib(3) = fib(2) + fib (1)
// fib(4) = fin(3) + fib (2)

// I don't even know how to start again :)
// well we maybe should make a function closure.. and that closure functions
// can call itself and yet store it's var between recursions

// I am going to brainstorm here how would I solve it in my brain
// so fib of 4
// it is
// fib(3) + fib(2)
// how do we get fib(3) = fib(2) + fib(1)
// how do we get fib(2) = fib(1) + fib(0)
// we know fib(0)=0 and fib(1)=1
// so we kind of have to count up but always slice only the last two
// recursion and a slice that contains the numbers as we find them
// a function closure and the slice is outside the function

func fibonacci(n int) int {
	//n passed to the function
	fibNums := make([]int, 3)
	fibNums[0] = 0
	fibNums[1] = 1

	// why i:=0 oh ok so you loop from 0 to i < n, yes.. and "n" is what we sent
	// the function when we call from main
	// now the problem is breaking down easier
	// function definitions before function calls :)... you see perl has spoiled me
	// I don't know if it matters , it does in C.. so wouldn't surprise me
	//func fibRecur(i int) int {
	//fibNums = append(fibNums,(fibNums[(i-1)] + fibNums[(i-2))]) // maybe?
	//bbl
	//ok won't probably finish it as it is getting late here, need to work tomorrow
	// but thanks, we can hack later
	//:)
	//}
	for i := 0; i < n; i++ {
		fibRecur(i) // don't know why I had func there, it is calling the function
	}

	// "n" comes from the params of the whole function
	return fibNums[n]
}

func main() {
	fmt.Println("Fibonacci numbers:")
	fibonacci(9)
}
