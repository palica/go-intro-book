package main

import "fmt"

func main() {
	for i := 1; i <= 10; i++ {
		fmt.Println("Variable i is now:", i)
		if i%2 == 0 {
			fmt.Println("And it is even")
		} else {
			fmt.Println("And it is odd")
		}
		switch i {
		case 0:
			fmt.Println("Zero")
		case 1:
			fmt.Println("One")
		case 2:
			fmt.Println("Two")
		case 3:
			fmt.Println("Three")
		default:
			fmt.Println("Unknown")
		}
	}
}
