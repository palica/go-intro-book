package main

import "fmt"

func main() {
	fmt.Println(1)
	fmt.Println(2)

	fmt.Println(`3
4
5`)
	fmt.Println("Doing it differently:")
	for i := 0; i <= 10; i++ {
		var pp string
    var number string
    switch i {
    case 1: number = "One"
    case 2: number = "Two"
    case 3: number = "Three"
    case 4: number = "Four"
    case 5: number = "Five"
    case 6: number = "Six"
    case 7: number = "Seven"
    case 8: number = "Eight"
    case 9: number = "Nine"
    case 10: number = "Ten"
    default: number = "Unknown number"
  }
		if i%2 == 0 {
			pp = "Even"
		} else {
			pp = "Odd"
		}
		fmt.Println(number, "is", pp)
	}
}
