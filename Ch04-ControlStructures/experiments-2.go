package main

import "fmt"

func main() {
	fmt.Println("Printing all numbers between 1-100 divisible by 3")
	for i := 0; i <= 100; i++ {
		if i%3 == 0 {
			fmt.Println("I=", i)
		}

	}
}
