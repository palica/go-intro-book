package main

import "fmt"

func main() {
	fmt.Println("Printing all numbers between 1-100 - 3 Fizz, 5 - Buzz")
	for i := 1; i <= 100; i++ {
		if i%3 == 0 && i%5 == 0 {
			fmt.Println("FizzBuzz")
		} else if i%3 == 0 {
			fmt.Println("Fizz")
		} else if i%5 == 0 {
			fmt.Println("Buzz")
		} else {
			fmt.Println("I=", i)
		}
	}
}
